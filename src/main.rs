use std::os::raw::c_int;

extern "C"  {
    fn hello_world();
    fn two_times(x:c_int)->c_int;
    fn print_test(t:Test);
    fn construct_array(length:c_int)-> *mut c_int;
    fn free_array(array:*mut c_int);
    fn construct_seq(length:c_int)-> IntVector;
    fn free_vec(iv:&IntVector);
    fn construct_testvec(contents:*const c_int, length: c_int)->TestVec;
    fn free_tv(tv:&TestVec);
    fn allocate_testvec(totlen:*mut c_int)->*mut TestVec;
    fn free_testvec_vec(tvs:*mut TestVec, length:c_int);
}

#[derive(Debug)]
#[repr(C)]
struct Test{
    inside:c_int,
}

#[derive(Debug)]
#[repr(C)]
struct IntVector{
    array:*const c_int,
    length: c_int,
}

#[derive(Debug)]
#[repr(C)]
struct TestVec{
    array:*const Test,
    length:c_int,
}

fn main(){
    // Nullary function.
    unsafe{
        hello_world();
    }
    // Unary function
    unsafe{
        println!("{}",two_times(10));
    }
    // Pass an object from Rust to C, as if it is
    // compatible in each other.
    let test = Test{inside:10};
    unsafe{
        print_test(test);
    };
    let length = 10;
    let mut array = unsafe{
        let a = construct_array(length);
        Vec::from_raw_parts(a, length as usize, length as usize)
    };
    println!("{:?}",array);
    unsafe{
        free_array(array.as_mut_ptr())
    };
    std::mem::forget(array);
    let array = unsafe{
        construct_seq(length)
    };
    eprintln!("{:?}",array);
    unsafe{
        free_vec(&array);
    }
    let mut totlen = 0;
    let testvec = unsafe{
        let contents = vec![1,2,3,4];
        construct_testvec(contents.as_ptr(), contents.len() as c_int)
    };
    unsafe{
        free_tv(&testvec);
    }
    println!("{:?}",testvec);
    let result = unsafe{
        allocate_testvec(&mut totlen)
    };
    eprintln!("{}",totlen);
    let mut result = unsafe{
        std::vec::Vec::from_raw_parts(result,totlen as usize,totlen as usize)
    };
    for tv in &result{
        println!("{:?}",tv);
    }
    unsafe{
        free_testvec_vec(result.as_mut_ptr(),totlen);
    };
    std::mem::forget(result);
}

