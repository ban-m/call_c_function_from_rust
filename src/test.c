#include <stdio.h>
#include <stdlib.h>

/* 
   As as first example, I define a nullary function 
   just printing "hello, world!"
*/

void hello_world(){
  fprintf(stdout,"hello from C\n");
}


/* 
   Next example is a unaray function 
   takeing one integer, printing its value, then 
   returning a integer two times larger than the arguments.
*/

int two_times(int x){
  fprintf(stdout,"%d",x);
  return 2*x;
}

/* 
   We can define  a struct and passed it to Rust,
   as if it is both Rust and C struct ,without any pain.
*/


typedef struct {
  int inner;
} Test;

void print_test(Test t){
  fprintf(stdout,"%d\n",t.inner);
}


/*
  Speaking of pointer and array, 
  it is necessary to pass the length of it,
  because without it Rust can not determine its length.
*/



typedef struct {
  int* array;
  int length;
} IntVector;

int * construct_array(int length){
  int * array = (int*)(malloc(sizeof(int) * length));
  for (int i = 0 ; i < length ; i ++){
    array[i] = i;
  }
  return array;
}

IntVector construct_seq(int length){
  int * array = construct_array(length);
  IntVector res = {array,length};
  return res;
}


/*
  Note that one should define free function 
  on C, not Rust. 
  It is because Rust don't know the precise memory allocator C using,
  and therefore can not call appropriate `free()` function.
 */

void free_array(int *array){
  free(array);
}

void free_vec(IntVector* vec){
  fprintf(stdout,"dropping from C...:%p\n",vec->array);
  free(vec->array);
}


/* 
   Without any pain, we can define a struct which has 
   an array of other struct inside. It is the 
   same as define "int-vector".
*/


typedef struct {
  Test *array;
  int length;
} TestVec;

TestVec construct_testvec(int* contents, int length){
  Test *array = (Test *)(malloc(sizeof(Test) * length));
  for (int i = 0 ; i < length; i ++){
    array[i].inner = contents[i];
  }
  TestVec tv = {array, length};
  return tv;
}

void free_tv(TestVec* tv){
  fprintf(stdout,"dropping from C...%p\n",tv->array);
  free(tv->array);
}

/*
Finally, we can pass to Rust an array, each element of which 
has an array inside.
As always, keep in mind that one should give `free` function
in the callee language.
*/

TestVec* allocate_testvec(int* totlen){
  int len = 10;
  TestVec *result = (TestVec*)(malloc(sizeof(TestVec)*len));
  for (int i = 0 ; i < len ; i ++){
    result[i].array = (Test*)(malloc(sizeof(Test)*i));
    for (int j = 0 ; j < i ; j ++){
      result[i].array[j].inner = i*j;
    }
    result[i].length = i;
  }
  *totlen = len;
  return result;
}

void free_testvec_vec(TestVec *tv, int length){
  for (int i = 0 ; i < length ; i ++){
    free_tv(&tv[i]);
  }
  free(tv);
}
